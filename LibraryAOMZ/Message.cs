﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryAOMZ
{
    [Serializable]
    public class MessageTerminal
    {
        public Int32 Length { get; set; }
        public Byte[] Body { get; set; }
    }
}
