﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryAOMZ
{
    public class Coordinates
    {
        public Double Latitude { get; set; }
        public Double Longitude { get; set; }

        public Int32 IdTerminal { get; set; }

        public Boolean InerEnable { get; set; }
    }
}
