﻿using LibraryAOMZ;
using Microsoft.AspNet.SignalR.Client;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SvpPool
{
    public class Apms
    {
        private readonly ConcurrentDictionary<Int32, Apm> _apms = new ConcurrentDictionary<Int32, Apm>();
        private readonly Queue<MessageTerminal> _messages = new Queue<MessageTerminal>();
        public readonly Dictionary<Int32, List<MessageTerminal>> _LogMessages = new Dictionary<Int32, List<MessageTerminal>>();
        private readonly Object _locker = new object();
        private readonly ServerHub server = new ServerHub();
        public Apms()
        {
        }

        public Task Connected(Int32 identity)
        {
            return server.Connected(identity);
        }
        public Task Disconnected(Int32 identity)
        {
            return server.Disconnected(identity);
        }
        
        public void AddOrUpdate(Apm apm)
        {
            _apms.AddOrUpdate(apm.GetHashCode(), apm, (x, y) => y);
        }

        public void Enqueue(Int32 bytesRead, Byte[] messageOld, CancellationToken token)
        {
            lock (_locker)
            {
                var array = new byte[messageOld.Length];
                messageOld.CopyTo(array,0);
                _messages.Enqueue(new MessageTerminal { Body = array, Length = bytesRead });
                Monitor.Pulse(_locker);
            }
        }
        public void ApmThread(CancellationToken token, IProgress<String> progress)
        {
            try
            {
                while (true)
                {
                    token.ThrowIfCancellationRequested();
                    MessageTerminal message;
                    lock (_locker)
                    {
                        while (_messages.Count == 0)
                        {
                            Monitor.Wait(_locker);
                        }
                        message = _messages.Dequeue();
                    }
                    Task.WaitAll(_apms.Select(apm1 => Task.Run(async () =>
                    {
                        try
                        {
                            await apm1.Value.Client.GetStream().WriteAsync(message.Body, 0, message.Length, token);
                        }
                        catch (InvalidOperationException)
                        {
                            apm1.Value.Client.Close();
                            RemoveApm(apm1);
                            progress.Report("Отключился АРМ");
                        }
                        catch (System.IO.IOException)
                        {
                            apm1.Value.Client.Close();
                            RemoveApm(apm1);
                            progress.Report("Отключился АРМ");
                        }
                        catch (OperationCanceledException)
                        {
                        }
                    }, token)).ToArray(), token);


                    string msg = Encoding.ASCII.GetString(message.Body, 0, message.Length);
                    string[] messages = msg.Split(new Char[] { '!' });
                    string[] split;

                   
                    for (int i = 0; i < messages.Count(); i++)
                    {
                        try
                        {
                            if (messages[i] != "")
                            {
                                split = messages[i].Split(new Char[] {'/'});
                                var lat = Convert.ToDouble(split[0]);
                                var lng = Convert.ToDouble(split[1]);
                                var id = Convert.ToInt32(split[2]);
                                var iner = Convert.ToBoolean(split[3]);
                                server.Send(new Coordinates
                                {
                                    Latitude = lat,
                                    Longitude = lng,
                                    IdTerminal = id,
                                    InerEnable = true
                                }).Wait(token);
                                List<MessageTerminal> lists;
                                _LogMessages.TryGetValue(id, out lists);
                                if (lists == null)
                                {
                                    _LogMessages.Add(id,new List<MessageTerminal>(){message});
                                }
                                else
                                {
                                    lists.Add(message);
                                }
                                
                            }
                        }
                        catch
                        {
                            continue;
                        }
                    }
                }
            }
            catch (OperationCanceledException)
            {
            }

        }

        private void RemoveApm(KeyValuePair<Int32, Apm> apm)
        {
            Apm ap;
            Int32 count = 0;
            while (!_apms.TryRemove(apm.Key, out ap))
            {
                Thread.Sleep(100);
                count++;
                if (count == 100)
                {
                    return;
                }
            };
        }

       
    }
}
