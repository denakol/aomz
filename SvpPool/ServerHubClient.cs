﻿using LibraryAOMZ;
using Microsoft.AspNet.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SvpPool
{
    public class ServerHub
    {
        private HubConnection hubConnection;
        private IHubProxy serverHubProxy;
        public ServerHub()
        {
            hubConnection = new HubConnection("http://176.58.61.237/signalr");
            serverHubProxy = hubConnection.CreateHubProxy("serverHub");
            hubConnection.Start().Wait();
        }

        public Task Send(Coordinates message)
        {
            return serverHubProxy.Invoke("SendMessage", message);
        }

        public Task Connected(Int32 identity)
        {
            return serverHubProxy.Invoke("Connected", new IdentytiTerminal() { Identity = identity });
        }

        public Task Disconnected(Int32 identity)
        {
            return serverHubProxy.Invoke("Disconnected", new IdentytiTerminal() { Identity = identity });
        }
    }
}
