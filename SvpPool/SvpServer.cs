﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using SvpPool.Extention;
namespace SvpPool
{
    public  class SvpServer
    {
        private readonly Apms _apms= new Apms();
        private volatile bool _stop;
        private  Int32 _countApm=0;
        private readonly TcpListener _listenerTerminal;
        private readonly TcpListener _listenerApm;
        private readonly CancellationTokenSource _token= new CancellationTokenSource();
        public SvpServer(IPEndPoint localEndPointTerminal, IPEndPoint localEndPointApm )
        {
            _listenerTerminal = new TcpListener(localEndPointTerminal);
            _listenerApm = new TcpListener(localEndPointApm);
        }
        public async void StartTerminal(IProgress<String> progress)
        {
            try
            {
                _listenerTerminal.Start();
                var count = 1;
                
                while (!_stop)
                {
                    var task= _listenerTerminal.AcceptTcpClientAsync();
                    var client = await task.WithCancellation(_token.Token);
                    client = await task.WithCancellation(_token.Token);
                    progress.Report("Подключился терминал СВП");
                    var thread = new Thread(() =>
                    {
                        var terminal = new Terminal(client);
                        var threadSvp = new TerminalThread(terminal, _apms, progress, _token.Token);
                        threadSvp.Start();
                    }) { IsBackground = true };
                    thread.Start();
                    progress.Report(count.ToString());
                    count++;
                }
            }
            catch (OperationCanceledException)
            {
                progress.Report("Поток приема терминалов завершен");
            }
            
        }

        public async void StartApm(IProgress<String> progress)
        {
            _listenerApm.Start();
            try
            {
              
                while (!_stop)
                {
                    var task = _listenerApm.AcceptTcpClientAsync();
                    var client = await task.WithCancellation(_token.Token);
                    progress.Report("Подключился АРМ");
                    var apm = new Apm(client);
                    _countApm++;
                    _apms.AddOrUpdate(apm);
                }
            }
            catch (OperationCanceledException)
            {
               progress.Report("Поток приема АРМ завершен");
            }
            
        }

        public void SendApm(IProgress<String> progress)
        {
            _apms.ApmThread(_token.Token,progress);
        }
        public void Stop()
        {
            _stop = true;
           _token.Cancel();
           FileStream fs = new FileStream("LOg.log", FileMode.Create);
           BinaryFormatter formatter = new BinaryFormatter();
           try
           {
               formatter.Serialize(fs, _apms._LogMessages);
           }
           finally
           {
               fs.Close();
           }
        }
    }


}
