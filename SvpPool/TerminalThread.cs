﻿using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using SvpPool.Extention;
using System.Threading.Tasks;
namespace SvpPool
{
    public class TerminalThread
    {

        private readonly Apms _apms;
        private readonly Terminal _terminal;
        private volatile bool _stop;
        private static IProgress<String> _progress;
        private CancellationToken _token;
        private object lockobj = new object();
        public TerminalThread(Terminal terminal,
            Apms apms,
            IProgress<String> progress, CancellationToken token
            )
        {
            _terminal = terminal;
            _apms = apms;
            _progress = progress;
            _token = token;
            _stop = false;
        }

        public async void Start()
        {
            var clientStream = _terminal._client.GetStream();
            var message = new byte[512];
            Int32 id=0;
            try
            {
                id=await AddToSignalr(clientStream);
                
                while (!_stop)
                {
                    _token.ThrowIfCancellationRequested();
                    var op = clientStream.ReadAsync(message, 0, 46, _token);
                    var bytesRead = await op.WithCancellation(_token);
                    if (bytesRead == 0)
                    {
                        throw new IOException();
                    }
                    var msg = Encoding.ASCII.GetString(message, 0, bytesRead);
                    _progress.Report(msg);
                    _apms.Enqueue(bytesRead, message, _token);
                }
            }
            catch (InvalidOperationException)
            {
                

            }
            catch (IOException)
            {
               
            }
            catch (OperationCanceledException)
            {
                  
            }
            finally
            {
                disconnected(id);
                _progress.Report("Отключился терминал" + id);
                _terminal._client.Close();
            }

        }

        private async Task<Int32> AddToSignalr(System.Net.Sockets.NetworkStream clientStream)
        {
            var message = new byte[4];
            var op = clientStream.ReadAsync(message, 0, 4, _token);
            var bytesRead = await op.WithCancellation(_token);
            var identity = BitConverter.ToInt32(message, 0);
            await _apms.Connected(identity);
            return identity;
        }
        private void disconnected(Int32 identity)
        {
            lock (lockobj)
            {
                _apms.Disconnected(identity);
            }
            
        }
    }
}
