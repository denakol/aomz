﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AOMZ.Models;
using Microsoft.AspNet.SignalR;
using LibraryAOMZ;
using System.Text;

namespace AOMZ.Hubs
{
    public class ServerHub : Hub
    {


        public void Connected(IdentytiTerminal identity)
        {

            using (var db = new AomzContext())
            {
                var terminal = db.Terminals.FirstOrDefault(x => x.TerminalID == identity.Identity);

                Clients.All.connectedTerminal(terminal);
            }
            
        }
        public void SendMessage(Coordinates message)
        {
            using (var db = new AomzContext())
            {
                var terminal = db.Terminals.FirstOrDefault(x => x.TerminalID == message.IdTerminal);
                db.Messages.Add(new MessageEntity()
                {
                    Latitude = message.Latitude,
                    Longitude = message.Longitude,
                    Terminal = terminal
                });

                db.SaveChanges();
                String name;
                Int32 ID;
                if (terminal == null)
                {
                    ID =-1;
                }
                else
                {
                    ID = terminal.TerminalID;
                }
                Clients.All.sendCoordinates(new {  message.Latitude, message.Longitude,ID});
            }

            
        }
    }
}