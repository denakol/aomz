namespace AOMZ.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Color : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Terminals", "ColorTreck", c => c.String());
            AddColumn("dbo.Terminals", "ColorMark", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Terminals", "ColorMark");
            DropColumn("dbo.Terminals", "ColorTreck");
        }
    }
}
