namespace AOMZ.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class rename : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Terminals", "ColorTrack", c => c.String());
            DropColumn("dbo.Terminals", "ColorTreck");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Terminals", "ColorTreck", c => c.String());
            DropColumn("dbo.Terminals", "ColorTrack");
        }
    }
}
