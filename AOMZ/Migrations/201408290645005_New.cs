namespace AOMZ.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class New : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MessageEntities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Latitude = c.Double(nullable: false),
                        Longitude = c.Double(nullable: false),
                        Terminal_TerminalID = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Terminals", t => t.Terminal_TerminalID)
                .Index(t => t.Terminal_TerminalID);
            
            CreateTable(
                "dbo.Terminals",
                c => new
                    {
                        TerminalID = c.Int(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.TerminalID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MessageEntities", "Terminal_TerminalID", "dbo.Terminals");
            DropIndex("dbo.MessageEntities", new[] { "Terminal_TerminalID" });
            DropTable("dbo.Terminals");
            DropTable("dbo.MessageEntities");
        }
    }
}
