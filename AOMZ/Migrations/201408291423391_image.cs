namespace AOMZ.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class image : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Terminals", "ImageUrl", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Terminals", "ImageUrl");
        }
    }
}
