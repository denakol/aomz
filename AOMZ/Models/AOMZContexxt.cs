﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace AOMZ.Models
{
    public class AomzContext:DbContext
    {
        public DbSet<Terminal> Terminals { get; set; }
        public DbSet<MessageEntity> Messages { get; set; } 
    }
}