﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AOMZ.Models
{
    public class MessageTerminals
    {
        public Double Latitude { get; set; }
        public Double Longitude { get; set; }

        public Int32 IdTerminal { get; set; }

        public String Name { get; set; }
        public Boolean InerEnable { get; set; }
    }
}