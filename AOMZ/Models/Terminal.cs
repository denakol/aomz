﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace AOMZ.Models
{
    public class Terminal
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Int32 TerminalID { get; set; }

        [DisplayName("Имя")]
        public String Name { get; set; }

        [JsonIgnore]
        public virtual ICollection<MessageEntity> Messages { get; set; }

        [DisplayName("Цвет трека")]
        public String ColorTrack { get; set; }
        [DisplayName("Цвет маркера")]
        public String ColorMark { get; set; }

        [DisplayName("Изображение маркера")]
        public String ImageUrl { get; set; }
    }
}