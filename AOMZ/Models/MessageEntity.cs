﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AOMZ.Models
{
    public class MessageEntity
    {
        public Int32 Id { get; set; }

        public virtual Terminal Terminal { get; set; }
        public Double Latitude { get; set; }
        public Double Longitude { get; set; }
    }
}