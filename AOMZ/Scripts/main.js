﻿'use strict'
$(function () {
    var server = $.connection.serverHub;
    $.connection.hub.start();
    var terminals = [];
    ymaps.ready(init);
    var myMap;
    function init() {
        myMap = new ymaps.Map("map", {
            center: [47.2136799496639, 38.9033603668213],
            zoom: 7,
            behaviors: ['default', 'scrollZoom'],
            controls: ["fullscreenControl", "geolocationControl", "routeEditor", "rulerControl"
            , "searchControl", "trafficControl", "typeSelector"]

        });
        var zoomControl = new ymaps.control.ZoomControl({ options: { position: { right: 10, top: 40 } } });

        myMap.controls.add(zoomControl);

    };



    server.on('sendCoordinates', function (message) {

        var result = $.grep(terminals, function (e) { return e.terminal.TerminalID == message.ID; });
        if (result.length == 0) {
        } else if (result.length == 1) {
            result[0].route.geometry.insert(result[0].route.geometry.getLength(), [message.Latitude, message.Longitude]);

            if (result[0] != null) {
                myMap.geoObjects.remove(result[0].mark);
            }

            var myPlacemark = new ymaps.Placemark([message.Latitude, message.Longitude],
                { hintContent: 'Собственный значок метки' },
                {
                    iconImageHref: '/content/' + result[0].terminal.ImageUrl + ".png",
                    // Размеры метки.
                    iconImageSize: [32, 32],
                    // Смещение левого верхнего угла иконки относительно
                    // её "ножки" (точки привязки).
                    iconImageOffset: [-16, -16],

                    iconLayout: 'default#image',
                    // Своё изображение иконки метки.
                });

            myMap.geoObjects.add(myPlacemark);
            result[0].mark = myPlacemark;
        } else {
            // multiple items found
        }

    });

    server.on('connectedTerminal', function (terminal) {
        var myPolyline;

        if (terminal.ColorMark == null) {
            terminal.ColorMark = "#FFFF00";
        }
        if (terminal.ColorTrack == null) {
            terminal.ColorTrack = "#FFFF00";
        }
        myPolyline = new ymaps.Polyline([
         // Указываем координаты вершин ломаной.
        ], {
            // Описываем свойства геообъекта.
            // Содержимое балуна.
            balloonContent: "Трек движения " + terminal.name
        }, {
            // Задаем опции геообъекта.
            // Отключаем кнопку закрытия балуна.
            balloonCloseButton: false,
            // Цвет линии.
            strokeColor: terminal.ColorTrack,
            // Ширина линии.
            strokeWidth: 10,
            // Коэффициент прозрачности.
            strokeOpacity: 0.7
        });
        myMap.geoObjects.add(myPolyline);
        terminals.push({ terminal: terminal, route: myPolyline, mark: null });


        $(".terminals").append('<div href="#" class="list-group-item" style="overflow: hidden;">' +
                                        '<h5 class="list-group-item-heading"><img style="margin-right:3px" src ="/content/' + terminal.ImageUrl + '.png"/>' + terminal.Name + '</h5>' +
                                       '<div style="float:left"  class="description-item">Идентификатор:' + terminal.TerminalID + '</div>' +
                                       '<div style="float:right" class="description-item">Цвет: <div  style="width:20px;height:15px;display:inline-block;background:' + terminal.ColorTrack + '"></div></div>' +
                                       '<div style="float:right"  class=" description-item">Слежение: <input type="checkbox"/></div>' +
                                        '</div>');
    });

    $('.input-daterange').datepicker({
    });


    function format(state) {
        var originalOption = state.element;
        return "<img class='flag' src='/content/" + $(originalOption).data('img') + ".png'/>" + state.text;
    }
    $("#selectObject").select2({
        formatResult: format,
        formatSelection: format,
        escapeMarkup: function (m) { return m; }
    });
});