﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AOMZ.Models;

namespace AOMZ.Controllers
{
    public class MessageEntitiesController : Controller
    {
        private AomzContext db = new AomzContext();

        // GET: MessageEntities
        public ActionResult Index()
        {
            return View(db.Messages.ToList());
        }

        // GET: MessageEntities/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MessageEntity messageEntity = db.Messages.Find(id);
            if (messageEntity == null)
            {
                return HttpNotFound();
            }
            return View(messageEntity);
        }

        // GET: MessageEntities/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: MessageEntities/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Latitude,Longitude")] MessageEntity messageEntity)
        {
            if (ModelState.IsValid)
            {
                db.Messages.Add(messageEntity);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(messageEntity);
        }

        // GET: MessageEntities/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MessageEntity messageEntity = db.Messages.Find(id);
            if (messageEntity == null)
            {
                return HttpNotFound();
            }
            return View(messageEntity);
        }

        // POST: MessageEntities/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Latitude,Longitude")] MessageEntity messageEntity)
        {
            if (ModelState.IsValid)
            {
                db.Entry(messageEntity).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(messageEntity);
        }

        // GET: MessageEntities/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MessageEntity messageEntity = db.Messages.Find(id);
            if (messageEntity == null)
            {
                return HttpNotFound();
            }
            return View(messageEntity);
        }

        // POST: MessageEntities/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            MessageEntity messageEntity = db.Messages.Find(id);
            db.Messages.Remove(messageEntity);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
