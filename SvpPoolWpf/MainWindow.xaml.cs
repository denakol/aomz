﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.Windows;
using LibraryAOMZ;
using SvpPool;

namespace SvpPoolWpf
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly SvpServer _server;
        private readonly Progress<String> _progress = new Progress<string>();

        private IPEndPoint point;
        public MainWindow()
        {
            InitializeComponent();
            _progress.ProgressChanged += Report;
            var ipAd = Dns.GetHostEntry(Dns.GetHostName());
            IPAddress ipv4 = ipAd.AddressList.First(x => x.AddressFamily == AddressFamily.InterNetwork);
            txt_ServerIp.Text = ipv4.ToString();
           txt_portSvp.Text = "2000";
            txt_portApm.Text = "3000";
            point = new IPEndPoint(ipv4, 2000);
            _server = new SvpServer(new IPEndPoint(ipv4, 2000), new IPEndPoint(ipv4, 3000));
            new Thread(() => _server.StartApm(_progress)) { IsBackground = true, Priority = ThreadPriority.Lowest }.Start();
            new Thread(() => _server.StartTerminal(_progress)) { IsBackground = true, Priority = ThreadPriority.Lowest }.Start();
            new Thread(() => _server.SendApm(_progress)) { IsBackground = true }.Start();
        }

        public void Report(object sender, String message)
        {
            txt.Text += message + "\n";
            txt.ScrollToEnd();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            _server.Stop();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            var window = new Window1();
            window.Show();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            new Thread(Send) { IsBackground = true, Priority = ThreadPriority.Lowest }.Start();
        }

        private void Send()
        {
            FileStream fs = new FileStream("LOg.log", FileMode.Open);
            Dictionary<Int32,List<MessageTerminal>> messages;
            try
            {
                BinaryFormatter formatter = new BinaryFormatter();

                // Deserialize the hashtable from the file and 
                // assign the reference to the local variable.
                messages = (Dictionary<Int32,List<MessageTerminal>>)formatter.Deserialize(fs);
            }
            catch (SerializationException e)
            {
                throw;
            }
            finally
            {
                fs.Close();
            }
            foreach (var message in messages)
            {
                new Thread(() => { Emulate(message.Value, message.Key); }) { IsBackground = true, Priority = ThreadPriority.Lowest }.Start();
               
            }
        }

        public void Emulate(List<MessageTerminal> message,Int32 id)
        {
            var rand = new Random();
            while (true)
            {
                var tcpClient = new TcpClient(point.Address.ToString(),point.Port);
                var array = BitConverter.GetBytes(id);
                var sendStream = tcpClient.GetStream();
                sendStream.Write(array, 0, array.Length);
                foreach (var item in message)
                {
                    sendStream.Write(item.Body,0,item.Length);
                    Thread.Sleep(rand.Next(700,2500));
                }
                sendStream.Close();
                tcpClient.Close();
                Thread.Sleep(rand.Next(2500));
            }
        }
    }
}
